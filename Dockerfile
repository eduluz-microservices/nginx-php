FROM  nginx:alpine

LABEL AUTHOR "Eduardo Luz <eduardo@eduardo-luz.com>"
LABEL PROJECT "Microservices Challenge" 
LABEL PROJECT_URL "https://gitlab.com/eduluz-microservices/nginx-php"


COPY _docker/web/scripts/* /root/scripts/


COPY _docker/web/conf/app.conf.tpl /root
COPY _docker/web/conf/nginx.conf.tpl /root

COPY _docker/web/static/index.html /srv/healthcheck/health/index.html

RUN chmod +x /root/scripts/*


HEALTHCHECK --interval=5s --timeout=10s --start-period=5s --retries=3 CMD [ "/root/scripts/healthcheck.sh" ]


CMD [ "/root/scripts/nginx.sh" ]

EXPOSE 8080
EXPOSE 80


COPY ./src /srv