user nginx;
worker_processes auto;
error_log /var/log/nginx/app.error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '${DOLLAR}remote_addr - ${DOLLAR}remote_user [${DOLLAR}time_local] "${DOLLAR}request" '
                      '${DOLLAR}status ${DOLLAR}body_bytes_sent "${DOLLAR}http_referer" '
                      '"${DOLLAR}http_user_agent" "${DOLLAR}http_x_forwarded_for"';

    access_log  /var/log/nginx/app.access.log  main;

    sendfile            on;
    # tcp_nopush          on;
    # tcp_nodelay         on;
    keepalive_timeout   60;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;
}
